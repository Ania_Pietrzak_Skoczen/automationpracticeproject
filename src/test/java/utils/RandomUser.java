package utils;

import com.github.javafaker.Faker;

import java.util.Calendar;

public class RandomUser {
    public String firstName;
    public String lastName;
    public String email;
    public Boolean isRegistered = false;
    public String password = "1qaz!QAZ";
    public int yearOfBirth;
    public int dayOfBirth;
    public int monthOfBirth;
    public String postalCode;
    public String state;
    public String address1;
    public String address2;
    public String city;
    public String company;
    public String country;
    public String phone;
    public String alias;

    public RandomUser() {
        Faker faker = new Faker();
        this.firstName = faker.name().firstName();
        this.lastName = faker.name().lastName();
        this.email = this.firstName + this.lastName + faker.random().nextInt(100000) + "@gmail.com";
        this.dayOfBirth = faker.date().birthday().getDay() +1;
        this.monthOfBirth = faker.date().birthday().getMonth() +1;
        this.yearOfBirth = 1990;
        this.postalCode = faker.address().zipCode().replace('-','1').substring(0,5);
        this.state = faker.address().state();
        this.city = faker.address().city();
        this.address1 = faker.address().streetAddress();
        this.address2 = faker.address().secondaryAddress();
        this.company = faker.company().name();
        this.country = faker.country().name();
        this.phone = faker.phoneNumber().cellPhone();
        this.alias = this.email.substring(0, this.email.indexOf("@"));
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", isRegistered=" + isRegistered +
                ", password='" + password + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                ", dayOfBirth=" + dayOfBirth +
                ", monthOfBirth=" + monthOfBirth +
                ", postalCode='" + postalCode + '\'' +
                ", state='" + state + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
