package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import java.util.List;

public class NavigationTest extends BaseTest {

    @Test
    void NavigationGotoBlousesShouldReturnExistingProductTest() {
        driver.get(BASE_URL);
        WebElement womenLink = driver.findElement(By.className("sf-with-ul"));

        Actions builder = new Actions(driver);
        builder.moveToElement(womenLink).perform();

        WebElement blousesLink = driver.findElement(
                By.linkText("Blouses"));
        builder.moveToElement(blousesLink).perform();
        blousesLink.click();

        List<WebElement> blouses = driver.findElements(By.cssSelector(".product_list .product-container"));
        Assertions.assertEquals(1, blouses.size());
    }

    @Test
    void NavigationToWomenTabShouldShowAvailableLinkGroupsTest() {
        driver.get(BASE_URL);
        WebElement womenLink = driver.findElement(By.className("sf-with-ul"));

        Actions builder = new Actions(driver);
        builder.moveToElement(womenLink).perform();

        List<WebElement> availableLinkGroups = driver.findElements(By.cssSelector(".submenu-container"));
        Assertions.assertEquals(2, availableLinkGroups.size());
    }
}
