Automation Test Final project

Technology stack:
 - Java 11+
 - Maven
 - Selenium WebDriver
 - jUnit 5.x
 
 Tests: 
 Registration: positive, negative
 Login: positive, negative, signout
 Order: make order
 Add to cart: add to cart one product, add to cart two different products
 Navigation: navigate to blouses, navigate to women
 Search: search for existing product, search for not existing product
 
