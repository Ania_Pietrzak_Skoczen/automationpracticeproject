package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @Test
    void shouldRegisterUserWhenMandatoryFieldsAreFilled(){
        driver.get(BASE_URL);
        driver.findElement(By.className("login")).click();

        RandomUser randomUser = new RandomUser();
        System.out.println(randomUser);

        driver.findElement(By.id("email_create")).sendKeys(randomUser.email);
        driver.findElement(By.id("email_create")).sendKeys(Keys.ENTER);

        driver.findElement(By.id("customer_firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("customer_lastname")).sendKeys(randomUser.lastName);
        //driver.findElement(By.id("email")).sendKeys(randomUser.email);
        driver.findElement(By.id("passwd")).sendKeys(randomUser.password);

        Select day = new Select(driver.findElement(By.id("days")));
        day.selectByValue(String.valueOf(randomUser.dayOfBirth));

        Select month = new Select(driver.findElement(By.id("months")));
        month.selectByValue(String.valueOf(randomUser.monthOfBirth));

        Select year = new Select(driver.findElement(By.id("years")));
        year.selectByValue(String.valueOf(randomUser.yearOfBirth));

        driver.findElement(By.id("firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("lastname")).sendKeys(randomUser.lastName);
        driver.findElement(By.id("company")).sendKeys(randomUser.company);
        driver.findElement(By.id("address1")).sendKeys(randomUser.address1);
        driver.findElement(By.id("city")).sendKeys(randomUser.city);
        driver.findElement(By.id("id_state")).sendKeys(randomUser.state);
        driver.findElement(By.id("postcode")).sendKeys(randomUser.postalCode);
        driver.findElement(By.id("id_country")).sendKeys(randomUser.country);
        driver.findElement(By.id("phone_mobile")).sendKeys(randomUser.phone);
        driver.findElement(By.id("alias")).sendKeys(randomUser.alias);

        driver.findElement(By.id("submitAccount")).sendKeys(Keys.ENTER);

        String logout = driver.findElement(By.className("logout")).getText();
        Assertions.assertEquals("Sign out", logout);
    }

    @Test
    void shouldNotRegisterUserWhenMandatoryFieldsAreNotFilled(){
        driver.get(BASE_URL);
        driver.findElement(By.className("login")).click();

        RandomUser randomUser = new RandomUser();
        System.out.println(randomUser);

        driver.findElement(By.id("email_create")).sendKeys(randomUser.email);
        driver.findElement(By.id("email_create")).sendKeys(Keys.ENTER);

        driver.findElement(By.id("customer_firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("customer_lastname")).sendKeys(randomUser.lastName);
        //driver.findElement(By.id("email")).sendKeys(randomUser.email);
        driver.findElement(By.id("passwd")).sendKeys(randomUser.password);

        Select day = new Select(driver.findElement(By.id("days")));
        day.selectByValue(String.valueOf(randomUser.dayOfBirth));

        Select month = new Select(driver.findElement(By.id("months")));
        month.selectByValue(String.valueOf(randomUser.monthOfBirth));

        Select year = new Select(driver.findElement(By.id("years")));
        year.selectByValue(String.valueOf(randomUser.yearOfBirth));

        driver.findElement(By.id("firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("lastname")).sendKeys(randomUser.lastName);
        driver.findElement(By.id("company")).sendKeys(randomUser.company);
        driver.findElement(By.id("address1")).sendKeys(randomUser.address1);
        driver.findElement(By.id("city")).sendKeys(randomUser.city);
        driver.findElement(By.id("id_state")).sendKeys(randomUser.state);
        //driver.findElement(By.id("postcode")).sendKeys(randomUser.postalCode);
        driver.findElement(By.id("id_country")).sendKeys(randomUser.country);
        driver.findElement(By.id("phone_mobile")).sendKeys(randomUser.phone);
        driver.findElement(By.id("alias")).sendKeys(randomUser.alias);

        driver.findElement(By.id("submitAccount")).sendKeys(Keys.ENTER);

        String error = driver.findElement(By.className("alert-danger")).getText();
        Assertions.assertEquals("There is 1 error\n" +
                "The Zip/Postal code you've entered is invalid. It must follow this format: 00000", error);
    }
}
