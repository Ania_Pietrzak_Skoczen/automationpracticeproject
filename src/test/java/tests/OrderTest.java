package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import utils.RandomUser;

public class OrderTest extends BaseTest{

    @Test
    void MakeOrder() {
        driver.get(BASE_URL);
        WebElement womenLink = driver.findElement(By.className("sf-with-ul"));

        Actions builder = new Actions(driver);
        builder.moveToElement(womenLink).perform();

        WebElement tshirts = driver.findElement(
                By.linkText("T-shirts"));
        builder.moveToElement(tshirts).perform();
        tshirts.click();

        WebElement product1 = driver.findElement(By.className("product-container"));
        builder.moveToElement(product1).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ajax_add_to_cart_button")));
        WebElement addToCart = driver.findElement(
                By.className("ajax_add_to_cart_button"));
        addToCart.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-default button button-medium']")));
        WebElement goToCart = driver.findElement(
                By.xpath("//a[@class='btn btn-default button button-medium']"));
        goToCart.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']")));
        driver.findElement(By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']")).click();

        CreateAccount();
        driver.findElement(By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']")).click();

        driver.findElement(By.xpath("//button[@class='button btn btn-default button-medium']")).click();

        driver.findElement(By.id("cgv")).click();

        driver.findElement(By.xpath("//button[@class='button btn btn-default standard-checkout button-medium']")).click();

        driver.findElement(By.className("bankwire")).click();

        driver.findElement(By.xpath("//button[@class='button btn btn-default button-medium']")).click();

        String confirmOrder = driver.findElement(By.cssSelector(".cheque-indent .dark")).getText();

        Assertions.assertEquals("Your order on My Store is complete.", confirmOrder);
    }

    private void CreateAccount() {
        driver.findElement(By.className("login")).click();
        RandomUser randomUser = new RandomUser();
        System.out.println(randomUser);

        driver.findElement(By.id("email_create")).sendKeys(randomUser.email);
        driver.findElement(By.id("email_create")).sendKeys(Keys.ENTER);

        driver.findElement(By.id("customer_firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("customer_lastname")).sendKeys(randomUser.lastName);
        driver.findElement(By.id("passwd")).sendKeys(randomUser.password);

        Select day = new Select(driver.findElement(By.id("days")));
        day.selectByValue(String.valueOf(randomUser.dayOfBirth));

        Select month = new Select(driver.findElement(By.id("months")));
        month.selectByValue(String.valueOf(randomUser.monthOfBirth));

        Select year = new Select(driver.findElement(By.id("years")));
        year.selectByValue(String.valueOf(randomUser.yearOfBirth));

        driver.findElement(By.id("firstname")).sendKeys(randomUser.firstName);
        driver.findElement(By.id("lastname")).sendKeys(randomUser.lastName);
        driver.findElement(By.id("company")).sendKeys(randomUser.company);
        driver.findElement(By.id("address1")).sendKeys(randomUser.address1);
        driver.findElement(By.id("city")).sendKeys(randomUser.city);
        driver.findElement(By.id("id_state")).sendKeys(randomUser.state);
        driver.findElement(By.id("postcode")).sendKeys(randomUser.postalCode);
        driver.findElement(By.id("id_country")).sendKeys(randomUser.country);
        driver.findElement(By.id("phone_mobile")).sendKeys(randomUser.phone);
        driver.findElement(By.id("alias")).sendKeys(randomUser.alias);

        driver.findElement(By.id("submitAccount")).sendKeys(Keys.ENTER);
    }
}
