package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class AddToCartTest extends BaseTest {

    @Test
    void AddToCartShouldAddProductToCart() {
        driver.get(BASE_URL);
        WebElement womenLink = driver.findElement(By.className("sf-with-ul"));

        Actions builder = new Actions(driver);
        builder.moveToElement(womenLink).perform();

        WebElement tshirts = driver.findElement(
                By.linkText("T-shirts"));
        builder.moveToElement(tshirts).perform();
        tshirts.click();

        WebElement product = driver.findElement(By.className("product-container"));
        builder.moveToElement(product).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ajax_add_to_cart_button")));
        WebElement addToCart = driver.findElement(
                By.className("ajax_add_to_cart_button"));
        addToCart.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-default button button-medium']")));
        WebElement goToCart = driver.findElement(
                By.xpath("//a[@class='btn btn-default button button-medium']"));
        goToCart.click();

        List<WebElement> cartItems = driver.findElements(By.cssSelector(".table .cart_item"));
        Assertions.assertEquals(1, cartItems.size());
    }

    @Test
    void AddTwoProductsToCartShouldAddProductsToCart() {
        driver.get(BASE_URL);
        WebElement womenLink = driver.findElement(By.className("sf-with-ul"));

        Actions builder = new Actions(driver);
        builder.moveToElement(womenLink).perform();

        WebElement tshirts = driver.findElement(
                By.linkText("T-shirts"));
        builder.moveToElement(tshirts).perform();
        tshirts.click();

        WebElement product1 = driver.findElement(By.className("product-container"));
        builder.moveToElement(product1).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ajax_add_to_cart_button")));
        WebElement addToCart = driver.findElement(
                By.className("ajax_add_to_cart_button"));
        addToCart.click();

        driver.findElement(
                By.cssSelector(".button-container .continue")).click();

        womenLink = driver.findElement(By.className("sf-with-ul"));
        builder.moveToElement(womenLink).perform();

        WebElement blouses = driver.findElement(
                By.linkText("Blouses"));
        builder.moveToElement(blouses).perform();
        blouses.click();

        WebElement product2 = driver.findElement(By.className("product-container"));
        builder.moveToElement(product2).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ajax_add_to_cart_button")));
        addToCart = driver.findElement(
                By.className("ajax_add_to_cart_button"));
        addToCart.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-default button button-medium']")));
        WebElement goToCart = driver.findElement(
                By.xpath("//a[@class='btn btn-default button button-medium']"));
        goToCart.click();

        List<WebElement> cartItems = driver.findElements(By.cssSelector(".table .cart_item"));
        Assertions.assertEquals(2, cartItems.size());
    }
}
